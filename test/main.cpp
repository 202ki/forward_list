#include <my/forward_list.h>
#include <gtest/gtest.h>

#include <forward_list>
#include <vector>
#include <numeric>
#include <string>

TEST(list, basic_test) {
    std::vector<int> a{1, 2, 3, 4, 5, 6};
    auto begin = a.begin();
    auto end = a.end();
    my::forward_list<int> list(begin, end);
    auto it_beg = list.begin();
    auto it_end = it_beg;
    std::advance(it_end, 4);
    int sum = std::accumulate(it_beg, it_end, 0);
    EXPECT_EQ(sum, 10);
    EXPECT_EQ(list.size(), 6);
    EXPECT_EQ(*it_end, 5);
    EXPECT_EQ(*it_end++, 5);
    EXPECT_EQ(*it_end, 6);
}
TEST(list, sort_test) {
    std::string str = "12345678910", s;
    my::forward_list<int> list;
    for (int i = 0; i < 10; ++i) {
        list.push_front(10 - i);
    }
    EXPECT_EQ(list.size(), 10);
    list.sort();
    auto it = list.begin();
    for (int i = 0; i < 10; ++i) {
        s += std::to_string(*it++);
    }
    EXPECT_EQ(s, str);
    std::vector<std::string> a{"ab", "abc", "weereb", "sbvcv", "svd"};
    auto new_list = my::forward_list<std::string>(a.begin(), a.end());
    new_list.sort();
    s.clear(), str.clear();
    std::sort(a.begin(), a.end());
    for (const auto& item: a) {
        str += item;
    }
    auto iter = new_list.begin();
    for (int i = 0; i < 5; ++i) {
        s += *iter++;
    }
    EXPECT_EQ(s, str);
}

TEST(list, list_of_lists_test) {
    std::vector<int> a = {5, 4, 3, 2, 1};
    my::forward_list<my::forward_list<int>> big_list;
    for (size_t i = 0; i < 5; ++i) {
        big_list.push_front(my::forward_list<int>(a.begin(), a.end()));
    }
    for (size_t i = 0; i < 5; ++i) {
        for (size_t j = 0; j < i; ++j) {
            big_list[i].pop_front();
        }
    }

    for (size_t i = 0; i < 5; ++i) {
        EXPECT_EQ(big_list[i].size(), 5 - i);
    }
    my::forward_list<int> lst, list;
    for (int i = 0; i < 5; ++i) {
        lst.push_front(i);
    }
    for (int i = 0; i < 5; ++i) {
        list.push_front(5 - i);
    }
}

TEST(list, test_push_and_pop) {
    my::forward_list<int> a, b;
    std::string ans = "0123456789", s1, s2;
    for (int i = 0; i < 10; ++i) {
        a.push_front(i);
        b.push_front(9 - i);
    }
    auto a_it = a.begin();
    auto b_it = b.begin();
    for (int i = 0; i < 10; ++i) {
        s2 += std::to_string(*a_it++);
        s1 += std::to_string(*b_it++);
    }
    EXPECT_EQ(s1, ans);
    std::reverse(ans.begin(), ans.end());
    EXPECT_EQ(s2, ans);
    for (int i = 0; i < 5; ++i) {
        a.pop_front();
        b.pop_front();
    }
    s1.clear(), s2.clear();
    a_it = a.begin();
    b_it = b.begin();
    for (int i = 0; i < 5; ++i) {
        s2 += std::to_string(*a_it++);
        s1 += std::to_string(*b_it++);
    }
    EXPECT_EQ(s1, "56789");
    EXPECT_EQ(s2, "43210");
}
TEST(list, assignments_test) {
    std::vector<int> a = {8, 8, 0, 0, 5, 5, 5, 3, 5, 3, 5};
    my::forward_list<int> lst(a.begin(), a.end()), lst1(a.rbegin(), a.rend());
    lst = lst1;
    lst.front() = 100;
    EXPECT_EQ(lst1.front(), 5);
    lst1.front() = 100;
    for (size_t i = 0; i < 11; ++i) {
        EXPECT_EQ(lst.front(), lst1.front());
        lst.pop_front();
        lst1.pop_front();
    }
    lst = my::forward_list<int>(a.begin(), a.begin() + 5);
    for (size_t i = 0; i < 5; ++i) {
        EXPECT_EQ(lst[i], *(a.begin() + static_cast<int>(i)));
    }
    lst1 = std::move(lst);
    for (size_t i = 0; i < 5; ++i) {
        EXPECT_EQ(lst1[i], *(a.begin() + static_cast<int>(i)));
    }
}

TEST(list, merge_and_concat_test) {
    std::vector<char> a = {'-', '-', '-', '.', '.', '.'};
    std::vector<char> b = {',', ',', ','};
    my::forward_list<char> lst(a.begin(), a.end()), lst1(b.begin(), b.end());
    lst.merge(lst1);
    std::string s;
    auto it = lst.begin();
    for (int i = 0; i < 9; ++i) {
        s += std::string(1, *it++);
    }
    EXPECT_EQ(s, ",,,---...");
    auto lst2 = lst;
    lst.concat(lst2);
    s.clear();
    it = lst.begin();
    for (int i = 0; i < 18; ++i) {
        s += std::string(1, *it++);
    }
    EXPECT_EQ(s, ",,,---...,,,---...");
    std::vector<int> tmp1, tmp2;
    for (int i = 0; i < 50; ++i) {
        tmp1.push_back(i);
        tmp2.push_back(i / 2);
    }
    std::forward_list<int> stl_first(tmp1.begin(), tmp1.end());
    my::forward_list<int> my_first(tmp1.begin(), tmp1.end());
    std::forward_list<int> stl_second(tmp2.begin(), tmp2.end());
    my::forward_list<int> my_second(tmp2.begin(), tmp2.end());
    stl_first.merge(stl_second);
    my_first.merge(my_second);
    for (int i = 0; i < 100; ++i) {
        EXPECT_EQ(my_first.front(), stl_first.front());
        my_first.pop_front();
        stl_first.pop_front();
    }
}
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

