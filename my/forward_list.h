#include <cstddef>
#include <algorithm>
#include <iterator>

namespace my
{
    template<typename T>
    struct Node
    {
        T value;
        Node *next;
    };

    template<typename T>
    using List = Node<T>*;

    template<typename T>
    struct ListUncons
    {
        Node<T> *head;
        List<T> tail;
    };

//-------------------Функции------------------

    template<typename T>
    List<T> cons(T const &value, List<T> const tail)
    {
        return new Node<T>{value, tail};
    }
    template<typename T>
    List<T> cons(T &&value, List<T> const tail)
    {
        return new Node<T>{static_cast<T &&>(value), tail};
    }

    template<typename T>
    ListUncons<T> uncons(List<T> const list)
    {
        return {list, list->next};
    }

    template<typename T, typename It>
    List<T> createList(It begin, It end)
    {
        return begin != end
               ? cons(*begin, createList<T>(std::next(begin), end))
               : nullptr;
    }

    template<typename T>
    void destroyList(List<T> list)
    {
        while(list != nullptr) {
            List<T> temp = list->next;
            delete list;
            list = temp;
        }
    }

    template<typename T>
    unsigned int listLength(List<T> list)
    {
        unsigned int length = 0u;
        for(; list != nullptr; list = list->next)
            ++length;
        return length;
    }

    template<typename T>
    List<T> prepend(Node<T> * const head, List<T> const tail)
    {
        head->next = tail;
        return head;
    }

    template<typename T>
    List<T> reverseList(List<T> list)
    {
        List<T> accum = nullptr;
        while(list != nullptr)
        {
            auto const [x, xs] = uncons(list);
            accum = prepend(x, accum);
            list = xs;
        }
        return accum;
    }

    template<typename T>
    List<T> concatList(List<T> const left, List<T> const right)
    {
        if (left == nullptr)
            return right;
        Node<T>* last = left;
        while (last->next != nullptr)
            last = last->next;
        last->next = right;
        return left;
    }

    template<typename T, typename Compare = std::less<T>>
    List<T> mergeList(List<T> left, List<T> right, Compare const& comp = Compare()) {
        List<T> result = nullptr;
        while (true)
        {
            if (left == nullptr) {
                auto temp = concatList(reverseList(result), right);
                return temp;
            }
            if (right == nullptr) {
                auto temp = concatList(reverseList(result), left);
                return temp;
            }

            ListUncons<T> const l = uncons(left);
            ListUncons<T> const r = uncons(right);
            if (comp(l.head->value, r.head->value))
            {
                result = prepend(l.head, result);
                left = l.tail;
            }
            else
            {
                result = prepend(r.head, result);
                right = r.tail;
            }
        }
    }

    template<typename T>
    Node<T>* listNth(List<T> list, std::size_t n)
    {
        for(; list != nullptr; list = list->next)
            if(0u == n--)
                break;
        return list;
    }

    template<typename T>
    class forward_list
    {
        List<T> list;

    public:
        ~forward_list() {
            destroyList(list);
        }

        forward_list() : list(nullptr) {}

        forward_list(forward_list<T> const &other)
                : forward_list(other.begin(), other.end())
        {}

        forward_list(forward_list<T>      &&other) noexcept : list(other.list) {
            other.list = nullptr;
        }

        forward_list(std::initializer_list<T> const &other)
                : forward_list(other.begin(), other.end())
        {}

        template<typename It>
        forward_list(It begin, It end) : list(createList<T>(begin, end)) {}

        forward_list &operator=(forward_list<T> other) {
            std::swap(list, other.list);
            return *this;
        }

        T       &front() {
            return list->value;
        }
        T const &front() const {
            return list->value;
        }

        void swap(forward_list<T> & other) noexcept {
            std::swap(list, other.list);
        }

        T       &operator[](std::size_t i)       { return listNth(list, i)->value; }
        T const &operator[](std::size_t i) const { return listNth(list, i)->value; }

        bool empty() const {
            return (list == nullptr);
        }

        std::size_t size() const {
            return listLength(list);
        }

        void reverse() {
            reverseList(list);
        }

        void concat(forward_list<T> &&other) {
            if (empty()) {
                *this = std::move(other);
                return;
            }
            list = concatList(list, other.list);
            other.list = nullptr;
        }

        void concat(forward_list<T> &other) {
            if (empty()) {
                *this = other;
                return;
            }
            list = concatList(list, other.list);
            other.list = nullptr;
        }

        void merge(forward_list<T> &&other) {
            if (empty()) {
                *this = std::move(other);
                return;
            }
            list = mergeList<T>(list, other.list);
            other.list = nullptr;
        }

        void merge(forward_list<T> &other) {
            if (empty()) {
                *this = other;
                return;
            }
            list = mergeList<T>(list, other.list);
            other.list = nullptr;
        }

        template<typename U>
        struct base_iterator
        {
            using difference_type = std::ptrdiff_t;
            using value_type = U;
            using pointer = U*;
            using reference = value_type&;
            using iterator_category = std::forward_iterator_tag;

            Node<T>* node;

            base_iterator<U> &operator++(   ) { node = node->next; return *this; }
            base_iterator<U>  operator++(int) { auto temp = node; node = node->next; return {temp}; }

            bool operator!=(base_iterator<U> const &other) const { return node != other.node; }
            bool operator==(base_iterator<U> const &other) const { return node == other.node; }

            U &operator* () const { return  node->value; }
            U *operator->() const { return &node->value; }
        };

        using       iterator = base_iterator<T      >;
        using const_iterator = base_iterator<T const>;

        iterator begin() {
            return {list};
        }

        const_iterator begin() const {
            return {list};
        }

        iterator end() {
            return {nullptr};
        }

        const_iterator end() const {
            return {nullptr};
        }

        void push_front(T const & value) {
            list = cons(value, list);
        }

        void push_front(T && value) {
            list = cons(std::move(value), list);
        }

        void pop_front() {
            auto const [x, xs] = uncons(list);
            list = xs;
            delete x;
        }

        void clear() {
            destroyList(list);
            list = nullptr;
        }

        template<typename Compare = std::less<T>>
        void sort(Compare const & comp = Compare()) {
            if (list == nullptr || list->next == nullptr) {
                return;
            }
            forward_list<T> left, right;
            Node<T>* curr = list;
            int size = 0;
            while (curr != nullptr) {
                ++size;
                if (size % 2 == 0) {
                    left.push_front(curr->value);
                } else {
                    right.push_front(curr->value);
                }
                curr = curr->next;
            }
            left.sort(comp);
            right.sort(comp);
            clear();
            list = mergeList<T, Compare>(left.list, right.list, comp);
            left.list = nullptr;
            right.list = nullptr;
        }
    };
}

